namespace BookingApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class HotelImagefix1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Hotel", "Image", c => c.Binary());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Hotel", "Image");
        }
    }
}
