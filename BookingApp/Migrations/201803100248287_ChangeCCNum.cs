namespace BookingApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeCCNum : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.CreditCard", "CCNum", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.CreditCard", "CCNum", c => c.Int(nullable: false));
        }
    }
}
