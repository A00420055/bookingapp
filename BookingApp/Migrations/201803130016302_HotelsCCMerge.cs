namespace BookingApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class HotelsCCMerge : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Hotel", "Image", c => c.Binary());
            AlterColumn("dbo.CreditCard", "CCNum", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.CreditCard", "CCNum", c => c.Int(nullable: false));
            DropColumn("dbo.Hotel", "Image");
        }
    }
}
