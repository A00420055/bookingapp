namespace BookingApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class HotelandReserv : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Reservation", "Id", c => c.Int(nullable: false));
            CreateIndex("dbo.Reservation", "Id");
            AddForeignKey("dbo.Reservation", "Id", "dbo.Contact", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Reservation", "Id", "dbo.Contact");
            DropIndex("dbo.Reservation", new[] { "Id" });
            DropColumn("dbo.Reservation", "Id");
        }
    }
}
