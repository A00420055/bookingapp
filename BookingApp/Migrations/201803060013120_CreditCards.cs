namespace BookingApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreditCards : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CreditCard",
                c => new
                    {
                        IdCard = c.Int(nullable: false, identity: true),
                        Id = c.Int(nullable: false),
                        CreditType = c.String(nullable: false),
                        CCName = c.String(nullable: false, maxLength: 50),
                        CCNum = c.Int(nullable: false),
                        ExpireDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.IdCard)
                .ForeignKey("dbo.Contact", t => t.Id, cascadeDelete: true)
                .Index(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CreditCard", "Id", "dbo.Contact");
            DropIndex("dbo.CreditCard", new[] { "Id" });
            DropTable("dbo.CreditCard");
        }
    }
}
