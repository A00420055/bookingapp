﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookingApp.Models
{
    [Table("Contact")]

    public class Contact
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required(ErrorMessage = "Last name is required")]
        [RegularExpression(@"^[A-Za-z'\s]{1,20}$", ErrorMessage = "Last Name can not contain digits or special charachters")]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "First name is required")]
        [RegularExpression(@"^[A-Za-z'\s]{1,20}$", ErrorMessage = "First Name can not contain digits or special charachters")]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Street number is required")]
        [Display(Name = "Street Number")]
        public int StreetNumber { get; set; }

        [Required(ErrorMessage = "City is required")]
        [RegularExpression(@"^[A-Za-z'\s]{1,20}$", ErrorMessage = "City can not contain digits or special charachters")]
        [Display(Name = "City")]
        public string City { get; set; }

        [Required(ErrorMessage = "Province or State is required")]
        [RegularExpression(@"^[A-Za-z'\s]{1,20}$", ErrorMessage = "Province or State can not contain digits or special charachters")]
        [Display(Name = "Province or State")]
        public string ProvinceState { get; set; }

        [Required(ErrorMessage = "Countryc is required")]
        [Display(Name = "Country")]
        public string Country { get; set; }

        public IEnumerable<SelectListItem> CountriesList { get; set; }

        [Required(ErrorMessage = "Postal Code required")]
        [Display(Name = "Postal Code")]
        [PostalCodeValidator(nameof(Country))]
        public string PostalCode { get; set; }

        [Required(ErrorMessage = "Phone number required")]
        [Display(Name = "Phone Number")]
        [PhoneValidator(nameof(Country))]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "The email address is required")]
        [Display(Name = "E-mail")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail is not valid")]
        public string Email { get; set; }
    }
}