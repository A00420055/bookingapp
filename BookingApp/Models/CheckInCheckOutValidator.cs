﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Web;

namespace BookingApp.Models
{
    public class CheckInCheckOutValidator : ValidationAttribute
    {
        private string checkInDateAttr;

        public CheckInCheckOutValidator(string checkInDateAttr)
        {
            this.checkInDateAttr = checkInDateAttr; 
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var checkOutDate = (DateTime)value;
            PropertyInfo propertyInfo = validationContext.ObjectType.GetProperty(checkInDateAttr);
            var checkInDate = (DateTime)propertyInfo.GetValue(validationContext.ObjectInstance);

            if (checkOutDate < checkInDate)
            {
                return new ValidationResult("Wrong Chek Out Date. The date should be after check in not before");
            }

            return ValidationResult.Success;
        }
    }
}