﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;

namespace BookingApp.Models
{
    
     

    public class Validator : ValidationAttribute
    {
        
        private string VType;

        public Validator(string ptype) : base("Invalid Credit Card Number")
        {
            this.VType = ptype;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var getCurrent = value.ToString();
            PropertyInfo propertyInfo = validationContext.ObjectType.GetProperty(VType);
           

            var Fieldvalue = (String)propertyInfo.GetValue(validationContext.ObjectInstance);
           


            if (getCurrent != null)
            {
               
                    if (Fieldvalue[0] == 'V')
                    {
                        if (getCurrent[0] == '4')
                        {
                            if ( getCurrent.Length == 16)
                            {
                                return ValidationResult.Success;
                            }
                            else
                            {
                                return new ValidationResult("Credit Card Number must have 16 numbers");
                            }
                        }
                        else
                        {
                            return new ValidationResult("Visa Credit Card must start with 4");
                        }

                    }
                    else
                    if (Fieldvalue[0] == 'M')
                    {
                        if ((getCurrent[0] == '5') && (getCurrent[1] == '5' || getCurrent[1] == '1'))
                        {
                            if (getCurrent.Length == 16)
                            {
                                return ValidationResult.Success;
                            }
                            else
                            {
                                return new ValidationResult("Credit Card Number must have 16 numbers");
                            }
                    }
                        else
                        {
                            return new ValidationResult("Master Card Credit Card must start with 55 or 51");
                        }
                }
                    else
                    if (Fieldvalue[0] == 'A')
                    {
                        if ((getCurrent[0] == '3') && (getCurrent[1] == '4' || getCurrent[1] == '7'))
                        {
                            if (getCurrent.Length == 15)
                            {
                                return ValidationResult.Success;
                            }
                            else
                            {
                                return new ValidationResult("Credit Card Number must have 15 numbers");
                            }
                    }
                        else
                        {
                            return new ValidationResult("American Express Credit Card must start with 34 or 37");
                        }
                }

                

                // Regex CanadianPostalCodeRgx = new Regex(@"^[a-zA-Z0-9]{3}[\s][a-zA-Z0-9]{3}$");
                // Regex USPostalCodeRgx = new Regex(@"^\d{5}(?:[-\s]\d{4})?$");
            }
                return ValidationResult.Success;
        }
    }
}