﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BookingApp.Models
{
    [Table("CreditCard")]
    public class CreditCard
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdCard { get; set; }

        // Foreign KEy
        public int Id { get; set; }
        public virtual Contact Contact { get; set; }

        [Required]
       
        public string CreditType { get; set; }

        [Required]
        [RegularExpression(@"^[A-Za-z''-'\s]{1,17}$",ErrorMessage = "Special Characters and Numbers not allowed")]
        [StringLength(50, ErrorMessage = "Card Holder Name cannot be longer than 50")]
        public string CCName { get; set; }

        [Required]
        //[Range(17,17)]
        //[RegularExpression(@"^[0-9A''-'\s]{1,17}$",
        // ErrorMessage = "Characters are not allowed.")]
        [Display(Name = "Credit Card Number")]
        [Validator(nameof(CreditType))]
        public string CCNum { get; set; }

        [Required]
        [DisplayFormat(DataFormatString = "{0:MM/yyyy}")]
       // [DataType(DataType.Date)]
        //[DisplayFormat(DataFormatString = "{0:MM-yyyy}", ApplyFormatInEditMode = true)]
        //[RegularExpression(@"^((0[1-9])|(1[0-2]))\/((2009)|(20[1-2][0-9]))$",
        // ErrorMessage = "Invalid Date")]
        public DateTime ExpireDate { get; set; }


    }
}