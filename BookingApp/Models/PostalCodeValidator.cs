﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;

namespace BookingApp.Models
{
    public class PostalCodeValidator : ValidationAttribute
    {
        private string countryAttr;

        public PostalCodeValidator(string countryAttr) : base("Invalid Postal Code Format")
        {
            this.countryAttr = countryAttr;
        }
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            PropertyInfo propertyInfo = validationContext.ObjectType.GetProperty(countryAttr);
            var countrySelection = (String)propertyInfo.GetValue(validationContext.ObjectInstance);

            Regex CanadianPostalCodeRgx = new Regex(@"^[a-zA-Z0-9]{3}[\s][a-zA-Z0-9]{3}$");
            Regex USPostalCodeRgx = new Regex(@"^\d{5}(?:[-\s]\d{4})?$");
            try
            {
                var postalCode = value.ToString();
                if (postalCode != null)
                {

                    if (countrySelection == "CANADA" && CanadianPostalCodeRgx.IsMatch(postalCode))
                    {
                        return ValidationResult.Success;
                    }
                    else if (countrySelection == "CANADA" && !CanadianPostalCodeRgx.IsMatch(postalCode))
                    {
                        return new ValidationResult("Invalid Postal Code Format For CANADA");
                    }

                    if (countrySelection == "USA" && USPostalCodeRgx.IsMatch(postalCode))
                    {
                        return ValidationResult.Success;
                    }
                    else if (countrySelection == "USA" && !USPostalCodeRgx.IsMatch(postalCode))
                    {
                        return new ValidationResult("Invalid Postal Code Format For USA");
                    }
                }
            }
            catch (Exception ex) {
                return new ValidationResult("Postal code is required");
            }

            return ValidationResult.Success;
        }
    }
}